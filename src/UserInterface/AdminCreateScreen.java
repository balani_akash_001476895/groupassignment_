/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.Abstract.User;
import Business.CustomerDirectory;
import Business.Users.Admin;
import Business.Users.Customer;
import Business.Users.Supplier;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.ButtonGroup;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author harshalneelkamal
 */
public class AdminCreateScreen extends javax.swing.JPanel {

    /**
     * Creates new form AdminScreen
     */
    private JPanel panelRight;
    private Admin admin;
    private ButtonGroup buttonGroup;

    public AdminCreateScreen(JPanel panelRight, Admin admin) {
        initComponents();
        this.panelRight = panelRight;
        this.admin = admin;
        buttonGroup();
        addUsernameInputVerifier();
        addPasswordInputVerifier();
        addRePasswordInputVerifier();
    }

    private void buttonGroup() {
        buttonGroup = new ButtonGroup();
        buttonGroup.add(radioCustomer);
        buttonGroup.add(radioSupplier);
    }

    private void addUsernameInputVerifier() {
        this.txtUser.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                JTextField source = (JTextField) input;
                String inputString = source.getText();
                String regex = "^([a-zA-Z0-9]+([_a-zA-Z0-9]+)*@[a-zA-Z0-9]+(\\.[a-zA-Z0-9]+)*(\\.[a-zA-Z]{2,}))$";

                if (inputString.isEmpty() || inputString.matches(regex)) {
                    return true;
                }
                return false;
            }

            @Override
            public boolean shouldYieldFocus(JComponent input) {
                boolean inputOK = verify(input);
                JTextField source = (JTextField) input;

                if (inputOK) {
                    return true;
                } else {
                    Toolkit.getDefaultToolkit().beep();
                    source.selectAll();
                    JOptionPane.showMessageDialog(txtUser, "Please enter valid Username: should be an email-ID with \"_\" and \"@\" as the only allowed special characters but should not start with an \"_\".");

                    return false;
                }
            }
        });
    }

    private void addPasswordInputVerifier() {
        this.jPasswordField.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                JPasswordField source = (JPasswordField) input;
                String inputString = String.valueOf(source.getPassword());
                String regex = "^([a-zA-Z0-9_$\\+]{3,})$";

                if (inputString.isEmpty() || inputString.matches(regex)) {
                    return true;
                }
                return false;
            }

            @Override
            public boolean shouldYieldFocus(JComponent input) {
                boolean inputOK = verify(input);
                JPasswordField source = (JPasswordField) input;

                if (inputOK) {
                    return true;
                } else {
                    Toolkit.getDefaultToolkit().beep();
                    source.selectAll();
                    JOptionPane.showMessageDialog(jPasswordField, "Please enter valid password: should be atleast 3 characters long and only allowed special character are \"+_$\"");

                    return false;
                }
            }
        });
    }

    private void addRePasswordInputVerifier() {
        this.jRePasswordField.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                JPasswordField source = (JPasswordField) input;
                String inputString = String.valueOf(source.getPassword());
                String regex = "^([a-zA-Z0-9_$\\+]{3,})$";

                if (inputString.isEmpty() || inputString.matches(regex)) {
                    return true;
                }
                return false;
            }

            @Override
            public boolean shouldYieldFocus(JComponent input) {
                boolean inputOK = verify(input);
                JPasswordField source = (JPasswordField) input;

                if (inputOK) {
                    return true;
                } else {
                    Toolkit.getDefaultToolkit().beep();
                    source.selectAll();
                    JOptionPane.showMessageDialog(jRePasswordField, "Please enter valid password: should be atleast 3 characters long and only allowed special character are \"+_$\"");

                    return false;
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCreate = new javax.swing.JButton();
        txtUser = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        radioCustomer = new javax.swing.JRadioButton();
        radioSupplier = new javax.swing.JRadioButton();
        btnBack = new javax.swing.JButton();
        jRePasswordField = new javax.swing.JPasswordField();
        jPasswordField = new javax.swing.JPasswordField();

        setBackground(new java.awt.Color(217, 213, 209));

        btnCreate.setText("Create");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });

        jLabel1.setText("username:");

        jLabel2.setText("password:");

        jLabel3.setText("re-enter password :");

        radioCustomer.setText("Customer");

        radioSupplier.setText("Supplier");

        btnBack.setText("< BACK");
        btnBack.setMaximumSize(new java.awt.Dimension(84, 29));
        btnBack.setMinimumSize(new java.awt.Dimension(84, 29));
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jRePasswordField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jRePasswordFieldKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnBack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtUser, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
                    .addComponent(btnCreate, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(radioSupplier)
                            .addComponent(radioCustomer)))
                    .addComponent(jRePasswordField)
                    .addComponent(jPasswordField))
                .addContainerGap(76, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRePasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addComponent(radioCustomer)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioSupplier)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCreate)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(61, Short.MAX_VALUE))
        );

        txtUser.getAccessibleContext().setAccessibleName("Username");
    }// </editor-fold>//GEN-END:initComponents

    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed
        if (txtUser.getText().isEmpty() || jPasswordField.getPassword().length == 0
                || jRePasswordField.getPassword().length == 0 || buttonGroup.getSelection() == null) {
            JOptionPane.showMessageDialog(null, "Please enter/select all fields");
            return;
        }

        if (!String.valueOf(jRePasswordField.getPassword()).equals(String.valueOf(jPasswordField.getPassword()))) {
            JOptionPane.showMessageDialog(null, "Please make sure the password and repassword match");
            return;
        }
        boolean userCustomer = false;
        if (radioCustomer.isSelected()) {
            userCustomer = true;
        }
        List<User> users = null;
        if (userCustomer) {
            users = admin.getCustDir().getCustomerList();
        } else {
            users = admin.getSuppDir().getSupplierList();
        }
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY/MM/DD");
        String username = txtUser.getText();
        char[] password = jPasswordField.getPassword();
        String pass = String.valueOf(password);

        boolean userNameMatched = false;

        for (User user : users) {
            if (username.equals(user.getUserName())) {
                userNameMatched = true;
                JOptionPane.showMessageDialog(btnCreate, "Entered username already exists, Please enter new Username");
                break;
            }
        }
        if (!userNameMatched) {
            if (userCustomer) {
                Customer customer = new Customer(pass, username, simpleDateFormat.format(date));
                users.add(customer);
            } else {
                Supplier supplier = new Supplier(pass, username);
                users.add(supplier);
            }
            JOptionPane.showMessageDialog(null, "Record Successfully Added.");
        }

        txtUser.setText(null);
        jPasswordField.setText(null);
        jRePasswordField.setText(null);
        buttonGroup.clearSelection();

    }//GEN-LAST:event_btnCreateActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed

        panelRight.remove(this);
            CardLayout layout = (CardLayout)panelRight.getLayout();
            Component[] componentArray = panelRight.getComponents();
            Component component = componentArray[componentArray.length - 1];
            AdminMainScreen manageFlightPanel = (AdminMainScreen)component;
            manageFlightPanel.populate();
            manageFlightPanel.populate2();
            
            layout.previous(panelRight);
    }//GEN-LAST:event_btnBackActionPerformed

    private void jRePasswordFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jRePasswordFieldKeyReleased
        if (String.valueOf(jRePasswordField.getPassword()).equals(String.valueOf(jPasswordField.getPassword()))) {
            jRePasswordField.setBackground(new java.awt.Color(255, 255, 255));
        } else {
            jRePasswordField.setBackground(new java.awt.Color(255, 204, 204));
        }
    }//GEN-LAST:event_jRePasswordFieldKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCreate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPasswordField jPasswordField;
    private javax.swing.JPasswordField jRePasswordField;
    private javax.swing.JRadioButton radioCustomer;
    private javax.swing.JRadioButton radioSupplier;
    private javax.swing.JTextField txtUser;
    // End of variables declaration//GEN-END:variables
}
