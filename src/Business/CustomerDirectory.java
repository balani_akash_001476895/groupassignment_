/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Abstract.User;
import Business.Users.Customer;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Date;
import java.util.List;

/**
 *
 * @author harshalneelkamal
 */
public class CustomerDirectory {
    
    private List<User> customerList;

     private Date myDate;


//    public Date getMyDate() {
//        return myDate;
//    }
//
//    public void setMyDate(Date myDate) {
//        this.myDate = myDate;
//    }

    
    public CustomerDirectory(){
        customerList = new ArrayList<>();
        
    }

    public List<User> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<User> customerList) {
        this.customerList = customerList;
    }
    
    public String indexOf1(List<User> customerList, String uname){
        //int index;
        for (User u : customerList){
            if(u.getUserName().equals(uname)){
                return u.toString();
            }
        }
        return null;
    }
    
    
    
}
