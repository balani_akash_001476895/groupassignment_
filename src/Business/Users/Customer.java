/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import Business.CustomerDirectory;
import java.util.Date;

/**
 *
 * @author harshalneelkamal
 */
public class Customer extends User implements Comparable<Customer> {

    private String date;

    public Customer(String password, String userName, String date) {
        super(password, userName, "CUSTOMER");
        this.date = date;
    }

    @Override
    public boolean verify(String password) {
        return this.getPassword().equals(password);
    }

    @Override
    public int compareTo(Customer o) {
        return this.getUserName().compareTo(o.getUserName());
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
